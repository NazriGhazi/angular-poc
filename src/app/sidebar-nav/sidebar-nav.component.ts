import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { faBars } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-sidebar-nav',
  templateUrl: './sidebar-nav.component.html',
  styleUrls: ['./sidebar-nav.component.css']
})
export class SidebarNavComponent implements OnInit {
  faBars= faBars;
  constructor() { }

  ngOnInit(): void {
    $(document).ready(function () {

      $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        
    });
      
      });

  }

}
