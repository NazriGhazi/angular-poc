import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ArtdirectionComponent } from './artdirection/artdirection.component';
import { AdCelcomfirstComponent } from './artdirection/ad-celcomfirst/ad-celcomfirst.component';
import { AdPhotographyComponent } from './artdirection/ad-photography/ad-photography.component';
const appRoutes: Routes=[
    {path:'', redirectTo: "/home", pathMatch:'full'},
    {path:'home', component: HomeComponent},
    {path:'artdirection', component: ArtdirectionComponent, children:[
        {path: 'celcomfirst', component: AdCelcomfirstComponent},
        {path: 'photography', component: AdPhotographyComponent}
    ]},
];


@NgModule({
 imports: [RouterModule.forRoot(appRoutes)],
 exports: [RouterModule]
})
export class AppRoutingModule{

}