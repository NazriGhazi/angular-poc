import { Component, OnInit } from '@angular/core';
import * as sal from 'sal.js';

@Component({
  selector: 'app-ad-photography',
  templateUrl: './ad-photography.component.html',
  styleUrls: ['./ad-photography.component.css']
})
export class AdPhotographyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    sal();
  }

}
