export class CelcomFirst{
    public imagePath: string;
    public description: string;

    constructor(imagePath:string, desc:string){
        this.imagePath = imagePath;
        this.description = desc;
    }
}

