import { CelcomFirst } from './celcomfirst.model';

export class CelcomFirstService{
    private celcomFirst: CelcomFirst[]=[
        new CelcomFirst('https://cdn-assets-cloud.frontify.com/local/frontify/eyJwYXRoIjoiXC9wdWJsaWNcL3VwbG9hZFwvc2NyZWVuc1wvMTY0MDkzXC81NmEyYjU3NGE3Yjc2ZjM2YjU2MjNmYmM3MTM1MDY0ZS0xNTIwOTk0MzExLmpwZyJ9:frontify:UXB_b6yMMC-TONfyOn36fXq36xaCnBY_VrWWEJn7TVs?width=2400','Inspiration from natures surroundings. Such as shapes, forms and lines.'),
        new CelcomFirst('https://cdn-assets-cloud.frontify.com/local/frontify/eyJwYXRoIjoiXC9wdWJsaWNcL3VwbG9hZFwvc2NyZWVuc1wvMTY0MDkzXC9kYmVjMWUxOTE1MTFlZDgzMzk4MzAwZmU3YzJiZGJlMC0xNTIwOTk0NDc0LmpwZyJ9:frontify:n3ybSObSlbsjo5EGKVsIS5hRriBMpmOEBd1Sol7pARE?width=2400','Looking deeper in the organism, we can discover a multitude of interesting lines that create a network of complex patterns that  bind each other to form a solid structure.'),
        new CelcomFirst('https://cdn-assets-cloud.frontify.com/local/frontify/eyJwYXRoIjoiXC9wdWJsaWNcL3VwbG9hZFwvc2NyZWVuc1wvMTY0MDkzXC84NmFjNTI4NmQ5ZDJkNWM1MTM2Yzg1NDdjNWZlMDdjNS0xNTIwOTk0NTg1LmpwZyJ9:frontify:JeDTJJ2tcsnFrnEbOzszsGJJ57hHe9Nkn87LzOHxnkk?width=2400','Topography/Aerial map of a city'),
        new CelcomFirst('https://cdn-assets-cloud.frontify.com/local/frontify/eyJwYXRoIjoiXC9wdWJsaWNcL3VwbG9hZFwvc2NyZWVuc1wvMTY0MDkzXC9mZjczZWM4OTlhOTgyMjhjYjk3OTJkMmNiNGE4Zjc2NS0xNTIwOTk3MzY2LmpwZyJ9:frontify:o3K2a-g3f0NV2OIO2pSavlwDeFp2vVpe9OpKxM4s_98?width=2400','The subtle textured background can be used as a calm and delighting mood that subconsciously tell the story of a network.')
    ];

    getCelcomFirst(){
        return this.celcomFirst.slice();
    }
}