import { Component, OnInit, OnChanges } from '@angular/core';
import { CelcomFirstService } from './celcomfirst.service';
import { CelcomFirst } from './celcomfirst.model';
import * as sal from 'sal.js';

@Component({
  selector: 'app-ad-celcomfirst',
  templateUrl: './ad-celcomfirst.component.html',
  styleUrls: ['./ad-celcomfirst.component.css']
})
export class AdCelcomfirstComponent implements OnInit,  OnChanges {
  celcomFirst: CelcomFirst[];

  constructor(private celcomFirstService: CelcomFirstService) { }

  ngOnInit(): void {
    sal();
    this.celcomFirst = this.celcomFirstService.getCelcomFirst();
  }

  ngOnChanges(){

  }

}
