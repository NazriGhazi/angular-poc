import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SidebarNavComponent } from './sidebar-nav/sidebar-nav.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ArtdirectionComponent } from './artdirection/artdirection.component'
import { AppRoutingModule } from './app-routing.module';
import { ParticlesDirective } from './particles.directive';
import { AdCelcomfirstComponent } from './artdirection/ad-celcomfirst/ad-celcomfirst.component';
import { CelcomFirstService } from './artdirection/ad-celcomfirst/celcomfirst.service';
import { AdPhotographyComponent } from './artdirection/ad-photography/ad-photography.component';


@NgModule({
  declarations: [
    AppComponent,
    SidebarNavComponent,
    HomeComponent,
    FooterComponent,
    ArtdirectionComponent,
    ParticlesDirective,
    AdCelcomfirstComponent,
    AdPhotographyComponent,
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppRoutingModule
  ],
  providers: [CelcomFirstService],
  bootstrap: [AppComponent]
})
export class AppModule { }
